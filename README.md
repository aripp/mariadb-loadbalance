catatan config load balance mariadb dinggo gae backup server database <br/>
langkah :

1. setting network server database <br/>
    edit file `$ sudo nano /etc/netplan/00-installer-config.yaml`
    ```
    #ip satu segmen
    #sesuaikan dengan interfacenya (enp0s8)
    enp0s8:
      dhcp4: false
      addresses: [192.168.1.10/24]
      gateway4: 192.168.1.1
      nameservers:
       addresses: [8.8.8.8, 8.8.4.4]
    ```
2. download mariadb pada semua server
    ```
    $ sudo apt-get install apt-transport-https curl
    $ sudo curl -o /etc/apt/trusted.gpg.d/mariadb_release_signing_key.asc 'https://mariadb.org/mariadb_release_signing_key.asc'
    $ sudo sh -c "echo 'deb https://download.nus.edu.sg/mirror/mariadb/repo/10.3/ubuntu focal main' >>/etc/apt/sources.list"
    $ sudo apt-get update
    $ sudo apt-get install mariadb-server
    ```
3. membuat user untuk haproxy dan user sql client (pada salah satu server saja)
    ```
    $ mysql -uroot -p
    mysql> CREATE USER 'haproxy'@'192.168.1.10';
    mysql> CREATE USER 'arip'@'%' IDENTIFIED BY 'okemantul';
    mysql> GRANT ALL PRIVILEGES ON *.* to 'arip'@'%' WITH GRANT OPTION;
    ```
    % artinya bisa login di manasaja (tidak terikat alamat ip) <br/>
    bintang.bintang artinya semua database dan semua table

4. setting galera cluster pada setiap server <br/>
    edit file `$ sudo nano /etc/mysql/mariadb.conf.d/galera.cnf`
    ```
    [mysqld]
    binlog_format=ROW
    default-storage-engine=innodb
    innodb_autoinc_lock_mode=2
    innodb_strict_mode=0
    skip-name-resolve
    bind-address=0.0.0.0

    #cluster conf
    wsrep_on=ON
    wsrep_provider=/usr/lib/galera/libgalera_smm.so
    wsrep_node_address=192.168.1.11
    wsrep_cluster_name=maincluster

    #ip address server database yang mau dibuat cluster
    wsrep_cluster_address=gcom://192.168.1.12,192.168.1.11
    wsrep_sst_method=rsync
    ```
5. run galera <br/> 
    stop service mariadb pada semua server jalankan <br/>
    `sudo service mysql stop` <br/>
    pada salah satu server (kalau ada server yang sudah ada databasenya) jalankan <br/>
    `sudo galera_new_cluster` <br/>
    server yang lain jalankan <br/>
    `sudo service mysql start`
6. setting haproxy (pada server haproxy) <br/>
    install dulu haproxynya <br/> 
    `sudo apt i haproxy` <br/>
    edit konfig haproxy <br/> 
    `sudo nano /etc/haproxy/haproxy.cfg`
    ```
    # untuk monitoring proxy web base (akses via ip server haproxy:8080/stats)
    listen stats
        bind ipserverhaproxy:8080
        stats enable
        stats refresh 30s
        stats hide-version
        stats show-node
        stats uri /stats

    # load balancing for galera cluster
    listen galera
        bind 0.0.0.0:3306
        balance source
        mode tcp
        option tcpka
        option mysql-check user haproxy

        #ipserver database di dalam cluster galera
        server db11 192.168.1.11:3306 check weight 1
        server db12 192.168.1.12:3306 check weight 1
    ```

referensi
1. https://staff.blog.ui.ac.id/jp/2016/07/22/mariadb-galera-cluster-haproxy-untuk-basisdata-lebih-handal/
2. https://belajarlinux.id/cara-instalasi-dan-konfigurasi-mariadb-galera-cluster-di-centos-8/
3. https://www.trijulian.web.id/index.php/2019/01/30/cara-load-balance-mysql-database-dengan-haproxy/
4. https://medium.com/@dataq/cara-recovery-mariadb-galera-cluster-efe16cb589b5 (kalau server db mati kabeh)
5. https://medium.com/biji-inovasi/create-dan-grant-user-di-mariadb-d6bfa7a67fc8

semoga ra dihapus sing nulis referensi

